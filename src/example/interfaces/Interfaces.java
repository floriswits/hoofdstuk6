package example.interfaces;

public class Interfaces {
    public static void main(String[] args) {
        // Een field in een interface is public static final, impliciet. Probeer aantalWervels maar te veranderen
        System.out.println(Gewervelden.AantalWervels);

//          Een methode in een interface kan sinds Java 8 een static implementatie hebben.
//          interface MetDefault{ static void metDefault(){ System.out.println("met Default"); } }
// ?VRAGEN WAAROM?          Dan is er GEEN verplichting om te implementeren MAAR de methode is NIET via een instantie aan te roepen. Uncomment maar;
//        Alleen via InterfaceNaam.methodeNaam(), dus MetDefault.metDefault();
//        Zoutwaterkrokodil snappie = new Zoutwaterkrokodil();
//        snappie.bijten();
        Reptielen.bijten();

        //Door het implementeren van een interface ontstaat er een IS-A relatie met de interface.
        Zoutwaterkrokodil z = new Zoutwaterkrokodil();
        System.out.println(z instanceof Reptielen);

        //gebruik van default methods interface koudbloedigen. Kijk maar.
        z.opwarmen();


    }

}
