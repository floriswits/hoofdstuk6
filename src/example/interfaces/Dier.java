package example.interfaces;

public class Dier implements Anatomie {
    //       Interface functies zijn impliciet public. Probeer public maar eens te veranderen in private bijv.
    //        Dus bij het implementeren van de methoden MOET er public voor de methode staan. EEN EXAMKILLER.

    @Override
    public void getAantalSpieren() {

    }
}
