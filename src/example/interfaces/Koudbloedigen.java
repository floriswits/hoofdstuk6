package example.interfaces;
//        Een methode in een interface kan sinds Java 8 een default implementatie hebben.
//          interface MetDefault{ default void metDefault(){ System.out.println("met Default"); } }
//          hiermee VERDWIJNT de verplichting om de methode te implementeren wanneer je interface implements. ZIe zoetwaterkrokodil

public interface Koudbloedigen {
    default void opwarmen(){
        System.out.println("ik lig in de zon");
    }
}
