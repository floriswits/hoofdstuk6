package example.overriding;

public class Collega extends Persoon{
    int leeftijd = 44;

    //dit is overriding - zelfde return type en signature
    void groeten(){
        System.out.println("MOIIIIIIII");
    }

    //geen overriding, maar overloading, want andere signature
    void groeten(String zegHoi){
        System.out.println(zegHoi);
    }

//    void zegDoei(){}

}
