package example.overriding;

public class Overriding {
    public static void main(String[] args) {
        //overriding is het geven van ander gedrag aan dezelfde methode. Bij overriding is EN de signature van belang EN
        //het returntype.

        Persoon p = new Persoon();
        Collega c = new Collega();

        p.groeten();
        c.groeten();
        c.groeten("hoi");

        //kind kan alles wat ouder kan
        p.zegDoei();
        c.zegDoei();
        // uncomment nu zegDoei() in collega. Run: en je ziet dat het gedrag wordt overridden

//?VRAGEN?     //Een ouder weet niet wie zijn kinderen zijn.
        //kind heeft leeftijd
        System.out.println(c.leeftijd);
        //persoon weet niet wat kind heeft -> uncomment onderstaand voor bewijs
        //p.leeftijd();

    }
}
