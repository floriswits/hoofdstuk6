package example.lambda;

import java.util.ArrayList;
import java.util.List;

//De lambda expressie is een argument waarmee functionaliteit kan worden meegegeven aan een toekomstig
//        verzamelingselement, zoals een stream.
//        Een lambda expressie ziet er als volgt uit
//        element -> element < 24
//        Als je een typering wilt weergeven zal dit tussen haken moeten, Ook als je niks met het element wilt doen, en dus
//        geen identifier toekent zul je haakjes voor de pijl moeten hebben.
//
//        (Hond deHond) -> deHond.aantalPootjes < 4
//        () -> 10
//        Als je meerdere statements wilt uitvoeren moet je achter de pijl accolades gebruiken.
//        element -> { System.out.println( element ); return element < 3; }
public class Lambda {
    public static void main(String[] args) {
        List<Hond> honden = new ArrayList<>();
        honden.add(new Hond("Fikkie"));
        honden.add(new Hond("Brutus"));
        honden.add(new Hond("Boomer"));

        honden.forEach(hond -> System.out.println(hond.naam));

        //?? klopt onderstaande als zijnde Lambda??
        for(Hond h : honden){
            System.out.println(h.naam);
        }

    }
}
