package example.polymorfisme;

public class Polymorfisme {
//    class Links{}
//    class Rechts extends Links{}
//    Links mijnDier = new Rechts();
//    Links mag groter zijn dan rechts.
//    Links bepaalt bij welke methoden en velden je kunt. Rechts bepaalt wat het object uiteindelijk kan en doet.
//    Polymorphisme zorgt ervoor dat de fields van Links worden gebruikt, en de methoden van Rechts.
//    Als een methoden een field gebruikt, gebruikt hij het field van Rechts. (Of tussen-level)

    public static void main(String[] args) {

        Boot boot = new Speedboot();
        Speedboot speedboot = new Speedboot();

        //reference type bepaalt bij welke methoden en velden je KUNT
        //Onderstaande kan dus niet, want reference type is Boot. Uncomment odnerstaande voor bewijs.
        //boot.nitro();

        //we kunnen wel bij de methoden van speedboot, want reference type is Speedboot.
        speedboot.nitro();

        //object bepaalt wat er moet gebeuren
        //boot is object Speedboot, dus vaart als een Speedboot (zie output)
        boot.varen();
        speedboot.varen();
    }
}
