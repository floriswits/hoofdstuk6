package example.polymorfisme;

public class Speedboot extends Boot {
    int vermogen = 900;

    void nitro() {
        System.out.println("VRRRRRROEEEEEMMMMMMM");
    }

    void varen() {
        System.out.println("varen met speedboot");
        System.out.println("vermogen: " + vermogen);
    }
}
