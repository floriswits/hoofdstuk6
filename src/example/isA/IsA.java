package example.isA;

public class IsA {
    //             Erving zorgt voor een Is-A relatie. Met instanceof kan de Is-A relatie gechecked worden.
//            (Bij klasses moet een Is-A relatie wel tot de opties behoren, anders compilefout)
//            (Bij interfaces hoeft een Is-A relatie niet tot de opties te behoren, slechts een runtime-error)

    public static void main(String[] args) {
        Appartement a = new Appartement();

        //check de is-a relatie
        if(a instanceof Woning){
            System.out.println("a is een Woning");
        }

        //interface:
        //geen compile fout bij ongerelateerde Interface, wel runtime error???
        if(a instanceof Vervuilend){
            Energieneutraal e = (Energieneutraal) a;
            System.out.println("a is energieneutraal");
        }


        //compile fout bij ongerelateerde Class, uncomment maar:
//        if(a instanceof Voetbal){
//            System.out.println("a is Appartement");
//        }

    }
}
