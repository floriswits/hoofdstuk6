package example.casting;
//Casting wordt ingezet om Links bij de functies van de kleinere Rechts of tussen-level te laten kunnen.
//        Casting is gebaseerd op vertrouwen. Als het mogelijk zou zijn, compilet het. Blijkt er toch geen Rechts in te zitten,
//        dan krijg je een runtimeError.
//        Rechts nieuwDier = (Rechts)mijnDier;
//        Als je een functie wil inzetten moet je extra haken zetten.
//        (Rechts)mijnDier.mijnMethode(); Cast het resultaat van de methode van mijn Dier. Waarschijnlijk FOUT.
//        ((Rechts)mijnDier).mijnMethode(); Cast het mijnDier naar een Rechts, en kan dan de methode van rechts
//        aanroepen.
public class Casting {
    public static void main(String[] args) {

        Plant c = new Cactus();
        Plant g = new Geldboom();
        //p.prikken is onbereikbaar. Uncomment onderstaande
        //p.prikken();

        //Op  r13 heeft p als referentietype Plant, waardoor je niet bij eigenschappen van Cactus kunt.
        //Door te casten beloven we dat c van het type Cactus is waardoor eigenschappen van Cactus beschikbaar komen.
        //prikken wordt nu wel bereikbaar
        Cactus cactusPlant = (Cactus) c;
        cactusPlant.prikken();

        //casten en methode aanroepen tegelijk
        ((Geldboom)g).makeItRain();

        //Door te casten beloven we dat c van het type Geldboom is waardoor eigenschappen van Geldboom beschikbaar komen.
        //Maar het is helemaal geen Geldboom, zie r14. Dus runtime error!
        Geldboom geldboom = (Geldboom) c;
        geldboom.makeItRain();
    }
}
